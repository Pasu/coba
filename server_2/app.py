import os
import subprocess
import zipfile
import pika
import sys
import time

from flask import Flask, request, render_template, jsonify, flash
from flask_cors import CORS
from werkzeug.utils import secure_filename
from threading import Thread

COMPRESS_FOLDER = 'Kompres'
FILES_FOLDER = 'File'

if not os.path.exists(COMPRESS_FOLDER):
    os.makedirs(COMPRESS_FOLDER)

if not os.path.exists(FILES_FOLDER):
    os.makedirs(FILES_FOLDER)

app = Flask(__name__)
app.config['COMPRESS_FOLDER'] = COMPRESS_FOLDER
app.config['FILES_FOLDER'] = FILES_FOLDER

CORS(app)

def zip_file(namaFile, tempatFile):
    zf = zipfile.ZipFile(os.path.join(app.config['COMPRESS_FOLDER'], namaFile + ".zip"), "w", zipfile.ZIP_DEFLATED)
    zf.write(os.path.join(tempatFile))

def async_func(namaFile, tempatFile, route_key):
    time.sleep(1)
    pika_credential = pika.PlainCredentials("0806444524", "0806444524")
    parameters = pika.ConnectionParameters(host ='152.118.148.95', port=5672, virtual_host='/0806444524', credentials=pika_credential)
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()

    channel.exchange_declare(exchange='1606896174', exchange_type='direct')

    thread = Thread(target = zip_file, args=(namaFile, tempatFile,))
    thread.start()
    current_progress = 0

    while thread.is_alive():
        try:
            zip_progress = os.path.getsize(os.path.join(app.config['COMPRESS_FOLDER'],namaFile + ".zip")) / os.path.getsize(os.path.join(tempatFile)) * 100
            if zip_progress > current_progress:
                message = str(current_progress)
                print(message)
                current_progress += 10
                channel.basic_publish(exchange='1606896174', routing_key = route_key, body=message)
        except Exception:
            pass

    if current_progress <= 100:   
        channel.basic_publish(exchange='1606896174', routing_key = route_key, body="100")
        print("100")

    connection.close()

@app.route('/', methods=['POST'])
def compress():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash("tidak ada file")
            return "Tidak ada file"
        file = request.files['file']
        if file:
            namaFile = secure_filename(file.filename)
            file.save(os.path.join(app.config['FILES_FOLDER'], namaFile))
            tempatFile = os.path.join(app.config['FILES_FOLDER'], namaFile)
            route_key = request.headers['X-ROUTING-KEY']
            thread = Thread(target=async_func, args=(namaFile, tempatFile, route_key))
            thread.start()

            return jsonify(status=200)

if __name__ == "__main__":
    app.run(debug=True)